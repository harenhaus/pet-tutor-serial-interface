# Pet Tutor Serial Interface

This repo contains an interface for the pet tutor system using serial commands. These programs send serial commands to the smart remote, which must be connected to the computer that is code is run on. The benefit of this code is that you can issue commands to your pet tutor by connecting to your home computer, regardless where you are, rather than in person with the smart remote.

There is both a GUI a command line version. The GUI was designed with PyQt designer; i.e., you can run `designer-qt4` (or the corresponding command on your OS) and open the `designer.ui`.

**Note that in the `myserial.py` file there are some options that may need to be changed. Most likely, the only one that will need to be changed is the port.** 
```
#!python

def open_serial():
    ser = serial.Serial()
    ser.baudrate = 115200
    ser.port = "/dev/ttyACM0"
    ser.timeout = 5.
    ser.write_timeout = 15.
```
On linux you can figure out the port assigned to the smart remote by plugging it in and out and checking `dmesg | grep -i tty`.



## Requirements

You must have Python installed to run this code. The GUI relies on PyQt4, but the command line version runs on just base python and [PySerial](https://pythonhosted.org/pyserial/) (which is easily installed).

## Running the Program
You can either run the GUI version or the command line version. The GUI version is better for an interactive training session, while the command line version is better for simply doing automatic feeds remotely.

#### GUI version
```
$ python gui.py
```
If you are able to run python code, the interface should be self-explanatory.

#### Command line version
```
$ python feed.py <num_feeds> <delay>
```


Have fun!
