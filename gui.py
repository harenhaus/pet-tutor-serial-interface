from PyQt4 import QtGui 
from PyQt4.QtCore import QThread, SIGNAL
import subprocess as sp
import time
import sys
import design
import myserial

MAX_FEEDS = 999  # Don't make your dog fat!
# ser = myserial.open_serial() 

class App(QtGui.QMainWindow, design.Ui_MainWindow):
    def __init__(self, parent=None):
        super(App, self).__init__(parent)
        self.setupUi(self)
        self.treat_counter = 0
        self.btnTreat.clicked.connect(self.treat)
        self.btnStart.clicked.connect(self.interval_treat)
        self.radioHr.toggled.connect(self.throw_bone)

    
    def throw_bone(self):
        if self.radioHr.isChecked():
            QtGui.QMessageBox.information(self, "Please!", "Throw the dog a bone!")
            self.radioSec.setChecked(True)


    def treat(self):
        self.thread = TreatThread()
        self.connect(self.thread, SIGNAL("increment()"), self.increment)
        self.connect(self.thread, SIGNAL("finished()"), self.done)
        self.thread.start()
        self.btnStop.setEnabled(True)
        self.btnStop.clicked.connect(self.thread.terminate)
        self.buttons_enabled(False)


    def buttons_enabled(self, val=True):
        self.btnStart.setEnabled(val)
        self.btnTreat.setEnabled(val)
        self.radioSec.setEnabled(val)
        self.radioMin.setEnabled(val)
        self.radioHr.setEnabled(val)
        self.spinBox.setEnabled(val)

    
    def interval_treat(self):
        interval = self.spinBox.value()
        if self.radioMin.isChecked():
            interval *= 60

        self.thread = TreatThread(interval)
        self.connect(self.thread, SIGNAL("increment()"), self.increment)
        self.connect(self.thread, SIGNAL("finished()"), self.done)
        self.thread.start()
        self.btnStop.setEnabled(True)
        self.btnStop.clicked.connect(self.thread.terminate)
        self.buttons_enabled(False)


    def done(self):
        self.btnStop.setEnabled(False)
        self.buttons_enabled(True)


    def increment(self):
        self.treat_counter += 1
        self.lcdNumber.display(self.treat_counter)





class TreatThread(QThread):
    def __init__(self, interval=None):
        QThread.__init__(self)
        self.interval = interval

    
    def __del__(self):
        self.wait()


    def run(self):
        iterations = MAX_FEEDS
        if self.interval == None:
            iterations = 1
        
        for i in range(iterations):
            st = time.time()
            
            # Execute command
            cmd = "echo 'FEEDING %d'" % (i,)
            sp.call(cmd, shell=True)
            #ser.write("00SGL_CYC\r")
            self.emit(SIGNAL("increment()"))
       
            # Pause for interval amount of time
            if i < iterations-1:
                duration = time.time() - st
                time.sleep(max(0, self.interval-duration))
        


def main():
    app = QtGui.QApplication(sys.argv)
    form = App()
    form.show()
    app.exec_()


if __name__ == '__main__':
    main()
