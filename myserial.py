import time
import serial

# Open named port with specified configurations
def open_serial():
    ser = serial.Serial()
    ser.baudrate = 115200
    ser.port = "/dev/ttyACM0"
    ser.timeout = 5.
    ser.write_timeout = 15.
    try:
        ser.open()
    except:
        print "Problem opening serial port. Check your superuser privilege."
        exit(1)
    time.sleep(0.25)
    ser.flushInput()    # clear any old feeder response output
    return ser
