import sys
import time
import datetime
import myserial

MAX_CYCLE = 100


print datetime.datetime.now()

# Number of feed cycles to perform
if len(sys.argv) != 2 and len(sys.argv) != 3:
    print "python feed.py <num_feeds> <delay>"
    exit(1)
num_feeds = int(sys.argv[1])
delay = 1
if len(sys.argv) == 3:
    delay = max(delay, int(sys.argv[2]))


ser = myserial.open_serial()

# Iteratively issue the feed command
count = 0
cycle = 1
while count<num_feeds and cycle<MAX_CYCLE:
    print "Feed cycle: %d" % (cycle)
    ser.write("00SGL_CYC\r")
    st = time.time()
    time.sleep(1)   # see if this helps

    # Read feeder response and print
    somethinghappened = False
    for i in range(2):
        output = ser.readline().strip()
        if output != "":
            print "  "+output
        if output == "00SGL_ERR":
            ser.close()
            exit(1)
        elif output == "":
            ser.close()
            time.sleep(5)
            ser = myserial.open_serial()
            break
        else:
            somethinghappened = True

    if somethinghappened:
        count += 1
        if count < num_feeds:
            duration = time.time() - st
            time.sleep(max(0, delay-duration))
    
    cycle += 1
    ser.flushInput()
    print "Total feeds performed: %d" % (count)
    print ""

    

# Close the port
ser.close()
